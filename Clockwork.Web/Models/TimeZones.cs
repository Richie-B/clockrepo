﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Clockwork.Web.Models
{
    public class TimeZones
    {
        public string ValueName { get; set; }
        public string DisplayName { get; set; }
    }
}