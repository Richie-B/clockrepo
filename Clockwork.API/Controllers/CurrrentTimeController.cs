﻿using System;
using Microsoft.AspNetCore.Mvc;
using Clockwork.API.Models;
using System.Linq;
using System.Web;
using System.Collections.Generic;

namespace Clockwork.API.Controllers
{
    public class CurrentTimeController : Controller
    {
        // GET api/currenttime
        [HttpGet()]
        [Route("api/[controller]/{timezone?}", Name = "Timezone")]
        public IActionResult Get(string timezone = "")
        {
            var utcTime = DateTime.UtcNow;
            var serverTime = DateTime.Now;
            TimeZoneInfo systemTimeZone = TimeZoneInfo.Local;
            TimeZoneInfo newTimeZone = null;
            DateTime? newUtcTime = null;
            DateTime? newServerTime = null;
            var ip = this.HttpContext.Connection.RemoteIpAddress.ToString();
            if (!string.IsNullOrEmpty(timezone))
            {
                newTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timezone);
                newUtcTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, newTimeZone);
                newServerTime = TimeZoneInfo.ConvertTime(serverTime, newTimeZone);
            }
            var returnVal = new CurrentTimeQuery
            {
                UTCTime = newUtcTime.HasValue ?  (DateTime)newUtcTime : utcTime,
                ClientIp = ip,
                Time = newServerTime.HasValue ? (DateTime)newServerTime : serverTime,
                Timezone = newTimeZone != null ? newTimeZone.DisplayName : systemTimeZone.DisplayName
            };

            using (var db = new ClockworkContext())
            {
                db.CurrentTimeQueries.Add(returnVal);
                var count = db.SaveChanges();
                Console.WriteLine("{0} records saved to database", count);

                Console.WriteLine();
                foreach (var CurrentTimeQuery in db.CurrentTimeQueries)
                {
                    Console.WriteLine(" - {0}", CurrentTimeQuery.UTCTime);
                }
            }

            return Ok(returnVal);
        }

        [HttpGet()]
        [Route("api/[controller]/GetAll")]
        public IActionResult GetAll()
        {
            using (var c = new ClockworkContext())
            {
                var results = c.CurrentTimeQueries.ToList();
                return Ok(results);
            }
        }
    }
}
